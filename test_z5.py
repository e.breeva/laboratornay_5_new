import z5
import pytest

lifts = []
for i in range(3):
    s1 = z5.lift(i + 1, 1)
    lifts.append(s1)
@pytest.mark.parametrize("a,res",[(5,[5,1]),(2,[2,2])])
def test_vizov(a,res):
    assert z5.vizov(lifts,a)==res